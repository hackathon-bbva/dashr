FROM rocker/r-ver:3.6.3

LABEL maintainer "Fabian Santander <hdmsantander@gmail.com>"

RUN apt-get update && apt-get install -y \
    sudo \
    libcairo2-dev \
    libcurl4-openssl-dev \
    libudunits2-dev \
    libssl-dev \
    libxml2-dev \
    libjpeg-dev \
    libxt-dev \
    libgdal-dev \
    libproj-dev \
    libgeos-dev \
    curl
    
RUN R -e "install.packages(c('dash', 'dashHtmlComponents', 'dashCoreComponents', 'plotly', 'dashTable', 'DBI', 'RPostgres', 'RPostgreSQL', 'rjson'), repos='https://cloud.r-project.org/', dependencies=TRUE)"

CMD ["/usr/local/bin/R"]
